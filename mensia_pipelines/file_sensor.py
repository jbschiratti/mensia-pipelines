"""File sensor on a filesystem (given by a connection)."""

import os
import os.path as op
import re

from airflow.contrib.hooks.fs_hook import FSHook
from airflow.operators.sensors import BaseSensorOperator
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults


class FileSensor(BaseSensorOperator):
    """Senses for a file (or multiple files) on a filesystem.

    Parameters
    ----------
    filepath : str
        Path (relatively to the base path set within the connection, see the
        `fs_conn_id` parameter) to a directory which contains the target
        files. If `filepath` is  not a directory or if the directory does not
        exist, the sensor will return `False`.

    filepattern : str
        Regular expression. The sensor will return `True` if there exist
        some files in `filepath` whose filename matches this regular
        expression.

    fs_conn_id : str (default: `fs_default`)
        Connection ID. The default connection ID `fs_default` refers to the
        local filesystem. The complete list of connection IDs can be found
        through Airflow UI with `Admin > Connections`.
    """
    template_fields = ('filepath',)
    ui_color = '#91818a'

    @apply_defaults
    def __init__(self,
                 filepath,
                 filepattern,
                 fs_conn_id='fs_default',
                 *args,
                 **kwargs):
        super(FileSensor, self).__init__(*args, **kwargs)
        self.filepath = filepath
        self.filepattern = filepattern
        self.fs_conn_id = fs_conn_id

    def poke(self, context):
        _hook = FSHook(conn_id=self.fs_conn_id)
        _basepath = _hook.get_path()
        fullpath = op.join(_basepath, self.filepath)
        if not op.isdir(fullpath):
            raise ValueError('The given `filepath` parameter ({}) shoud refer '
                             'to a directory on the filesystem specified by '
                             'the `fs_conn_id` parameter '
                             '({}).'.format(self.filepath, self.fs_conn_id))
        else:
            all_fnames = os.listdir(fullpath)
            _fnames = (fname for fname in all_fnames if op.isfile(fname))
            _fp = re.compile(self.filepattern)
            valid_fnames = [op.join(fullpath, fname) for fname in _fnames
                            if re.match(_fp, fname)]
            if not valid_fnames:
                return False
            else:
                context['task_instance'].xcom_push('filenames', valid_fnames)
                return True


class FileSensorPlugin(AirflowPlugin):
    name = "filesensor_plugin"
    operators = [FileSensor]
