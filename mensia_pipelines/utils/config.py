"""Configuration functions."""

import json
import os
import os.path as op
from warnings import warn

VALID_CONFIG_KEYS = ['NEURORT_RELEASE_PATH']


def _load_config(config_path, raise_error=False):
    """Load a JSON config file."""
    with open(config_path, 'r') as fid:
        try:
            config = json.load(fid)
        except ValueError:
            msg = ('The mensia-pipelines config file (%s) is not a valid JSON '
                   'file and might be corrupted' % config_path)
            if raise_error:
                raise RuntimeError(msg)
            warn(msg)
            config = dict()
    return config


def get_config_path(home_dir=None):
    """Get path to the JSON config file.

    Parameters
    ----------
    home_dir : str or None (default: None)
        The folder that contains the `.mensia-pipelines` config folder.
        If None, the function will try to find it automatically.

    Returns
    -------
    config_path : str
        The path to the mne-python configuration file. On windows, this
        will be `%USERPROFILE%\.mensia-pipelines\mensia-pipelines.json`. On any
        other system, this will be `~/.mensia-pipelines/mensia-pipelines.json`.
    """
    val = op.join(_find_home_dir(home_dir=home_dir),
                  'mensia-pipelines.json')
    return val


def get_config(key=None, default=None, raise_error=False, home_dir=None):
    """Get key from environment variables or config file.

    Parameters
    ----------
    key : str or None (default: None)
        The key to look for. If None, all the config parameters present in
        environment variables or the path are returned.

    default : str or None (default: None)
        Value to return if the key is not found.

    raise_error : bool (default: False)
        If True, raise an error if the key is not found (instead of returning
        default).

    home_dir : str or None (default: None)
        The folder which contains the .mensia-pipelines config file.

    Returns
    -------
    value : dict or string or None
        The key(s) value(s).
    """
    if key is not None and not isinstance(key, str):
        raise TypeError('The `key` parameter does not have the right type. '
                        'Got {}. `key` can either be None or be a '
                        'string.'.format(type(key)))

    # Check if the key is among the environment variables
    if key is not None and key in os.environ:
        return os.environ[key]

    # If not, check if the key is in the config file
    config_path = get_config_path(home_dir=home_dir)
    if not op.isfile(config_path):
        config = {}
    else:
        config = _load_config(config_path)

    if key is None:
        env_keys = (set(config).union(VALID_CONFIG_KEYS).
                    intersection(os.environ))
        config.update({key: os.environ[key] for key in env_keys})
        return config
    elif raise_error is True and key not in config:
        raise KeyError('Key {} is not found in environment or in the '
                       'config file. Try adding this key using the '
                       '`set_config` function.'.format(key))
    else:
        return config.get(key, default)


def set_config(key, value, home_dir=None, set_env=True):
    """Set a given key in the config file (and possibly environment also).

    Parameters
    ----------
    key : str
        The key to set.

    value : str or None
        The value to assign to the preference key. If None, the key is
        deleted.

    home_dir : str or None (default: None)
        The folder that contains the .config file.

    set_env : bool (default: True)
        If True, update `os.environ` in addition to the config file.
    """
    if not isinstance(key, str):
        raise TypeError('The `key` parameter must be a string. '
                        'Got {} instead.'.format(type(key)))
    elif key not in VALID_CONFIG_KEYS:
        raise ValueError('The given `key` parameter ({}) is invalid. '
                         'Valid keys are: {}.'.format(key, VALID_CONFIG_KEYS))

    config_path = get_config_path(home_dir=home_dir)
    if op.isfile(config_path):
        config = _load_config(config_path, raise_error=True)
    else:
        config = dict()
        warn_msg = 'Creating a new config file in {}.'.format(config_path)
        warn(warn_msg)
    if value is None:
        config.pop(key, None)
        if set_env and key in os.environ:
            del os.environ[key]
    else:
        config[key] = value
        if set_env:
            os.environ[key] = value

    directory = op.dirname(config_path)
    if not op.isdir(directory):
        os.mkdir(directory)
    with open(config_path, 'w') as fid:
        json.dump(config, fid, sort_keys=True, indent=0)


def _find_home_dir(home_dir=None):
    """Get path to extra data (config, tables, etc.)."""
    if home_dir is None:
        if 'nt' == os.name.lower():
            if op.isdir(op.join(os.getenv('APPDATA'), '.mensia-pipelines')):
                home_dir = os.getenv('APPDATA')
            else:
                home_dir = os.getenv('USERPROFILE')
        else:
            home_dir = os.path.expanduser('~')

        if home_dir is None:
            raise ValueError('The mensia-pipelines config file path could '
                             'not be determined.')
    return op.join(home_dir, '.mensia-pipelines')
