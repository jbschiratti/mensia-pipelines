import os
import unittest

import mock
from airflow.configuration import load_test_config
from airflow.exceptions import AirflowSensorTimeout
from airflow.models import TaskInstance, DAG
from airflow.utils.timezone import datetime

from mensia_pipelines.file_sensor import FileSensor

TEST_DAG_ID = 'unit_tests'
DEFAULT_DATE = datetime(2019, 1, 1)


class FileSensorTest(unittest.TestCase):
    """Class for testing the FileSensor."""

    def setUp(self):
        load_test_config()
        args = {
            'owner': 'airflow',
            'start_date': DEFAULT_DATE,
            'provide_context': True
        }
        dag = DAG(TEST_DAG_ID + '_dag', default_args=args)
        dag.schedule_interval = '@once'
        self.dag = dag

    @mock.patch('mensia_pipelines.file_sensor.os.listdir')
    @mock.patch('mensia_pipelines.file_sensor.op.isfile',
                new=lambda s: True if s.endswith('.txt') else False)
    def test_simple(self, mock_listdir):
        mock_listdir.return_value = ['test_1_file.txt',
                                     'test_2_doc.txt',
                                     'datadir',
                                     'some_other_file.txt']
        task = FileSensor(
            task_id='simple',
            filepath=os.curdir,
            filepattern='test_1_file.txt',
            fs_conn_id='fs_default',
            dag=self.dag,
            timeout=0)
        ti = TaskInstance(task=task, execution_date=DEFAULT_DATE)
        task.execute(ti.get_template_context())

        # Get filenames pushed to XCom and check against expected filenames
        res = ti.xcom_pull(task_ids='simple', key='filenames')
        self.assertEqual([fname.split(os.sep)[-1] for fname in res],
                         ['test_1_file.txt'])

    @mock.patch('mensia_pipelines.file_sensor.os.listdir')
    @mock.patch('mensia_pipelines.file_sensor.op.isfile',
                new=lambda s: True if s.endswith('.txt') else False)
    def test_file_not_exist(self, mock_listdir):
        mock_listdir.return_value = ['test_1_file.txt',
                                     'test_2_doc.txt',
                                     'datadir',
                                     'some_other_file.txt']
        task = FileSensor(
            task_id='file_not_exist',
            filepath=os.curdir,
            filepattern='myfile.ext',
            fs_conn_id='fs_default',
            dag=self.dag,
            timeout=0)
        ti = TaskInstance(task=task, execution_date=DEFAULT_DATE)
        with self.assertRaises(AirflowSensorTimeout):
            task.execute(ti.get_template_context())

    @mock.patch('mensia_pipelines.file_sensor.os.listdir')
    @mock.patch('mensia_pipelines.file_sensor.op.isfile',
                new=lambda s: True if s.endswith('.txt') else False)
    def test_regex(self, mock_listdir):
        mock_listdir.return_value = ['test_1_file.txt',
                                     'test_2_doc.txt',
                                     'datadir',
                                     'some_other_file.txt']
        task = FileSensor(
            task_id='regex',
            filepath=os.curdir,
            filepattern='test_[1-2].*',
            fs_conn_id='fs_default',
            dag=self.dag,
            timeout=0)
        ti = TaskInstance(task=task, execution_date=DEFAULT_DATE)
        task.execute(ti.get_template_context())

        # Get filenames pushed to XCom and check against expected filenames
        res = ti.xcom_pull(task_ids='regex', key='filenames')
        self.assertEqual([fname.split(os.sep)[-1] for fname in res],
                         ['test_1_file.txt', 'test_2_doc.txt'])


if __name__ == '__main__':

    unittest.main()
